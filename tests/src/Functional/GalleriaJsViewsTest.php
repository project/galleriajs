<?php

namespace Drupal\Tests\galleriajs\Functional;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\Tests\BrowserTestBase;

/**
 * Galleria JS views test.
 *
 * @group galleriajs
 */
class GalleriaJsViewsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The default display settings to use for the formatters.
   *
   * @var array
   */
  protected $defaultSettings = ['timezone_override' => ''];

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_test',
    'galleriajs',
    'views',
    'galleriajs_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create test data:
    for ($i = 0; $i < 5; $i++) {
      EntityTest::create([
        'id' => 'Test ' . $i,
        'label' => 'Test ' . $i,
      ])->save();
    }
  }

  /**
   * Tests Date Time Flat Pickr functionality.
   */
  public function testDateTimeFlatPickrWidget() {
    // Display creation form.
    $this->drupalGet('galleriaja-test');
    $this->assertSession()->elementTextContains('xpath', '//div[@class="galleriajs-images galleria"]/*/span', '1');
    $this->assertSession()->elementExists('xpath', '//div[@class="galleriajs"]');
    $this->assertSession()->elementExists('xpath', '//div[@class="galleriajs-images galleria"]');
  }

}
