
 GalleriaJS
----------------------------

Integrating Galleria with Views.

This module will display a view of images using the Galleria jQuery plugin
available from http://galleria.io.

Galleria
— A JavaScript gallery for the Fastidious

Galleria is a JavaScript image gallery unlike anything else. It can take a
simple list of images and turn it into a foundation of multiple intelligent
gallery designs, suitable for any project.

Dependencies
------------
1. [Galleria.io](https://galleria.io/) library file


Installation
------------

1. Download the Galleria jQuery plugin
 (https://galleria.io/ version 1.6 or higher is recommended) and extract 
 the file under "libraries".
2. Download and enable the module.
 
Installation via Composer
-------------------------
It is assumed you are installing Drupal through Composer using the Drupal
Composer facade. See https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies#drupal-packagist

Before you add the module using composer, you should add an installer path
so that the Gallery.io JavaScript library is installed in the correct location.
You might have an entry similar to below in your composer.json already if
you had used [drupal-composer/drupal-project](https://github.com/drupal-composer/drupal-project)
to create your project.
```
    "extra": {
        "installer-paths": {
            "web/libraries/{$name}": ["type:drupal-library"]
        }
    }
```
Where `web/libraries/` is the path to the libraries directory relative to your
_project_ root. Add `galleriajs/galleria` repository.
```
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": " galleriajs/galleria",
                "version": "1.6.1",
                "type": "drupal-library",
                "dist": {
                    "url": "https://github.com/galleriajs/galleria/archive/1.6.1.zip",
                    "type": "zip"
                }
            }
        },
    ],
```

Then, run the following composer command:

```
composer require galleriajs/galleria
composer require drupal/galleriajs
```
