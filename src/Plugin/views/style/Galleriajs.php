<?php

namespace Drupal\galleriajs\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Style plugin to render each item in a galleriajs.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "galleriajs",
 *   title = @Translation("Galleriajs"),
 *   help = @Translation("Display the results as a galleriajs."),
 *   theme = "galleriajs_main_frame",
 *   display_types = {"normal"}
 * )
 */
class Galleriajs extends StylePluginBase {
  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * Does the style plugin for itself support to add fields to it's output.
   *
   * This option only makes sense on style plugins without row plugins, like
   * for example table.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = [
      'theme' => ['default' => 'classic'],
      'autoplay' => ['default' => FALSE],
      'autoplay_ms' => ['default' => 3000],
      'carousel' => ['default' => TRUE],
      'carousel_follow' => ['default' => TRUE],
      'carousel_speed' => ['default' => 200],
      'carousel_steps' => ['default' => 'auto'],
      'clicknext' => ['default' => FALSE],
      'dummy' => ['default' => ''],
      'custom_theme_options' => [
        'contains' => [
          'custom_theme' => ['default' => ''],
          'custom_theme_path' => ['default' => ''],
        ],
      ],
      'easing' => ['default' => 'galleria'],
      'fullscreen_crop' => ['default' => ''],
      'fullscreen_double_tap' => ['default' => TRUE],
      'fullscreen_transition' => ['default' => ''],
      'height' => ['default' => 0],
      'idle_mode' => ['default' => TRUE],
      'idle_time' => ['default' => 3000],
      'idle_speed' => ['default' => 200],
      'image_crop' => ['default' => FALSE],
      'image_margin' => ['default' => 0],
      'image_pan' => ['default' => FALSE],
      'image_pan_smoothness' => ['default' => 12],
      'image_position' => ['default' => 'center'],
      'initial_transition' => ['default' => ''],
      'layer_follow' => ['default' => TRUE],
      'lightbox' => ['default' => FALSE],
      'lightbox_fade_speed' => ['default' => 200],
      'lightbox_transition_speed' => ['default' => 300],
      'max_scale_ratio' => ['default' => ''],
      'min_scale_ratio' => ['default' => ''],
      'overlay_opacity' => ['default' => 0.85],
      'overlay_background' => ['default' => '#0b0b0b'],
      'pause_on_interaction' => ['default' => TRUE],
      'popup_links' => ['default' => FALSE],
      'preload' => ['default' => 2],
      'queue' => ['default' => TRUE],
      'responsive' => ['default' => FALSE],
      'show' => ['default' => 0],
      'show_info' => ['default' => TRUE],
      'show_counter' => ['default' => TRUE],
      'show_imagenav' => ['default' => TRUE],
      'swipe' => ['default' => TRUE],
      'thumb_crop' => ['default' => FALSE],
      'thumb_fit' => ['default' => TRUE],
      'thumb_margin' => ['default' => 0],
      'thumb_quality' => ['default' => TRUE],
      'thumbnails' => ['default' => TRUE],
      'touch_transition' => ['default' => ''],
      'transition' => ['default' => 'fade'],
      'transition_speed' => ['default' => 400],
      'width' => ['default' => 'auto'],
      'advanced' => [
        'contains' => [
          'avoid_flash_of_content' => ['default' => TRUE],
          'data_config' => ['default' => ''],
          'data_selector' => ['default' => 'img'],
          'data_source' => ['default' => ''],
          'debug' => ['default' => TRUE],
          'extend' => ['default' => ''],
          'history' => ['default' => 0],
          'keep_source' => ['default' => FALSE],
          'strip_images' => ['default' => TRUE],
        ],
      ],
    ];

    // Add options:
    return $options + parent::defineOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // Create options form:
    $galleria = \Drupal::service('library.discovery')->getLibraryByName('galleriajs', 'galleria.js');
    if (!isset($galleria['js'][0]['data']) || !file_exists($galleria['js'][0]['data'])) {
      $form['galleriajs']['no_galleria_js'] = [
        '#markup' => '<div style="color: red">' . $this->t('You need to install the Galleria plugin. Create a directory in libraries (which should be in your Drupal root folder, if not create the same) called galleria. You can find the plugin at @url.',
          [
            '@url' => Link::fromTextAndUrl('http://galleria.io', Url::FromUri('http://galleria.io', ['attributes' => ['target' => '_blank']]))->toString(),
          ]) . '</div>',
      ];
    }

    $transition_options = [
      'fade' => $this->t('Fade'),
      'flash' => $this->t('Flash'),
      'pulse' => $this->t('Pulse'),
      'slide' => $this->t('Slide'),
      'fadeslide' => $this->t('Fade/Slide'),
    ];

    $crop_options = [
      'width' => $this->t('Width'),
      'height' => $this->t('Height'),
      0 => $this->t('False'),
      1 => $this->t('True'),
    ];

    $form['theme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Galleria theme to load on display. If you choose the %custom option, you must specify the path to your custom JavaScript theme below.',
        [
          '%custom' => $this->t('<Custom>'),
        ]
      ),
      '#default_value' => $this->options['theme'],
      '#options' => [
        'custom' => $this->t('Custom (specify options below)'),
        'miniml' => $this->t('Miniml'),
        'twelve' => $this->t('Twelve'),
        'fullscreen' => $this->t('Fullscreen'),
        'classic' => $this->t('Classic'),
        'folio' => $this->t('Folio'),
        'azur' => $this->t('Azur'),
      ],
    ];

    $form['custom_theme_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom theme options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['custom_theme_options']['custom_theme'] = [
      '#type' => 'textfield',
      '#title' => t('Custom theme'),
      '#description' => $this->t('Leave this blank unless you wish to override the theme used by the plugin. You should specify the name of the custom theme here, and enter its path below. See the @theme_api for how to create your own Galleria JavaScript themes.',
        [
          '@theme_api' => Link::fromTextAndUrl($this->t('Galleria JavaScript theme API'), Url::FromUri('https://docs.galleria.io/article/39-creating-a-theme', ['attributes' => ['target' => '_blank']]))->toString(),
          '%custom' => $this->t('<Custom>'),
        ]
      ),
      '#default_value' => $this->options['custom_theme_options']['custom_theme'],
    ];

    $form['custom_theme_options']['custom_theme_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom theme path'),
      '#description' => $this->t("Leave this blank unless you're overriding the theme. See the @theme_api for how to create your own Galleria JavaScript themes.",
        [
          '@theme_api' => Link::fromTextAndUrl($this->t('Galleria JavaScript theme API'), Url::FromUri('https://docs.galleria.io/article/39-creating-a-theme', ['attributes' => ['target' => '_blank']]))->toString(),
        ]
      ),
      '#default_value' => $this->options['custom_theme_options']['custom_theme_path'],
    ];

    $form['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => t('Autoplay'),
      '#description' => $this->t('If checked, then the slide show will begin rotating after the specified time below.'),
      '#default_value' => $this->options['autoplay'],
    ];

    $form['autoplay_ms'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autoplay time'),
      '#description' => $this->t('If the slide show is set to autoplay above, then begin after this many miliseconds.'),
      '#default_value' => $this->options['autoplay_ms'],
    ];
    $form['carousel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Carousel'),
      '#description' => $this->t('If checked, this will activate the carousel when needed. Otherwise it will not appear at all.'),
      '#default_value' => $this->options['carousel'],
    ];
    $form['carousel_follow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Carousel follow'),
      '#description' => $this->t('If checked, the carousel will follow the active image.'),
      '#default_value' => $this->options['carousel_follow'],
    ];
    $form['carousel_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carousel speed'),
      '#description' => $this->t('The slide speed of the carousel in milliseconds.'),
      '#default_value' => $this->options['carousel_speed'],
    ];
    $form['carousel_steps'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carousel steps'),
      '#description' => $this->t('The number of "steps" the carousel will slide when navigating between available thumbnails. Specifying %auto will move the carousel as many steps as there are visible thumbnails.', ['%auto' => 'auto']),
      '#default_value' => $this->options['carousel_steps'],
    ];
    $form['clicknext'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Click next'),
      '#description' => $this->t('This options adds a click event over the stage that navigates to the next image in the gallery. Useful for mobile browsers and other simpler applications.'),
      '#default_value' => $this->options['clicknext'],
    ];
    $form['dummy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dummy'),
      '#description' => $this->t('This option allows you to define an image that should be shown if Galleria can’t find the original image.'),
      '#default_value' => $this->options['dummy'],
    ];
    $form['easing'] = [
      '#type' => 'select',
      '#title' => $this->t('Easing'),
      '#description' => $this->t('You can use this option to control the animation easing on a global level in Galleria.'),
      '#default_value' => $this->options['easing'],
      '#options' => [
        'galleria' => 'galleria',
        'galleriain' => 'galleriaIn',
        'galleriaout' => 'galleriaOut',
      ],
    ];
    $form['fullscreen_crop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fullscreen crop'),
      '#description' => $this->t('Sets how Galleria should crop when in fullscreen mode. See imageCrop for cropping options.'),
      '#default_value' => $this->options['fullscreen_crop'],
      '#options' => $crop_options,
    ];
    $form['fullscreen_double_tap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fullscreen double tap'),
      '#description' => $this->t('This options listens for the double-tap event on touch devices and toggle fullscreen mode if it happens.'),
      '#default_value' => $this->options['fullscreen_double_tap'],
    ];
    $form['fullscreen_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Fullscreen transition'),
      '#description' => $this->t('Defines a different transition for fullscreen mode. Some transitions are less smooth in fullscreen mode, this option allows you to set a different transition when in fullscreen mode.'),
      '#default_value' => $this->options['fullscreen_transition'],
      '#options' => $transition_options,
    ];
    $form['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#description' => $this->t('This will set a height to the gallery. If you set this to %auto and no CSS height is found, Galleria will automatically add a 16/9 ratio as a fallback.', ['%auto' => 'auto']),
      '#default_value' => $this->options['height'],
      '#required' => TRUE,
    ];
    $form['idle_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Idle mode'),
      '#description' => $this->t('Global option for turning on/off idle mode. Eache gallery enters idle mode after certain amount of time and themes behave differently when this happens, f.ex clears the stage from distractions.'),
      '#default_value' => $this->options['idle_mode'],
    ];
    $form['idle_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Idle time'),
      '#description' => $this->t('If you are adding elements into idle mode using the .addIdleState() method, you can control the delay before Galleria falls into Idle mode using this option.'),
      '#default_value' => $this->options['idle_time'],
    ];
    $form['idle_speed'] = [
      '#type' => 'textfield',
      '#title' => t('Idle speed'),
      '#description' => t('If you are adding elements into idle mode using the addIdleState() method, you can control the animation speed of the idle elements.'),
      '#default_value' => $this->options['idle_speed'],
    ];
    $form['image_crop'] = [
      '#type' => 'select',
      '#title' => $this->t('Image crop'),
      '#description' => $this->t('Defines how the main image will be cropped inside it’s container.'),
      '#default_value' => $this->options['image_crop'],
      '#options' => $crop_options,
    ];
    $form['image_margin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image margin'),
      '#description' => $this->t('Sets a margin between the image and stage. Specify the number of pixels.'),
      '#default_value' => $this->options['image_margin'],
    ];
    $form['image_pan'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Image pan'),
      '#description' => $this->t('Galleria comes with a built-in panning effect. The effect is sometimes useful if you have cropped images and want to let the users pan across the stage to see the entire image.'),
      '#default_value' => $this->options['image_pan'],
    ];
    $form['image_pan_smoothness'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image pan smoothness'),
      '#description' => $this->t('This value sets how “smooth” the image pan movement should be when setting image_pan to true. The higher value, the smoother effect but also CPU consuming.'),
      '#default_value' => $this->options['image_pan_smoothness'],
    ];
    $form['image_position'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image position'),
      '#description' => $this->t("Positions the main image. Works like the CSS background-position property; i.e., 'top right' or '20% 100%'. You can use keywords, percents or pixels. The first value is the horizontal position and the second is the vertical. Read more at @read_more.",
        [
          '@read_more' => Link::fromTextAndUrl('http://www.w3.org/TR/REC-CSS1/#background-position', Url::FromUri('http://www.w3.org/TR/REC-CSS1/#background-position', ['attributes' => ['target' => '_blank']]))->toString(),
        ]
      ),
      '#default_value' => $this->options['image_position'],
    ];
    $form['initial_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Initial transition'),
      '#description' => $this->t('Defines a different transition to show on the first image.'),
      '#default_value' => $this->options['initial_transition'],
      '#options' => $transition_options,
    ];
    $form['layer_follow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Layer follow'),
      '#description' => $this->t('If checked, the source HTML will be left intact, which will also create clickable images of each image inside the source. Useful for building custom thumbnails and still have galleria control the gallery.'),
      '#default_value' => $this->options['layer_follow'],
    ];
    $form['lightbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lightbox'),
      '#description' => $this->t('This option acts as a helper for attaching a lightbox when the user clicks on an image. If you have a link defined for the image, the link will take precedence.'),
      '#default_value' => $this->options['lightbox'],
    ];
    $form['lightbox_fade_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lightbox fade speed'),
      '#description' => $this->t('When calling .showLightbox() the lightbox will animate and fade the images and captions. This value controls how fast they should fade in milliseconds.'),
      '#default_value' => $this->options['lightbox_fade_speed'],
    ];
    $form['lightbox_transition_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lightbox transition speed'),
      '#description' => $this->t('When calling .showLightbox() the lightbox will animate the white square before displaying the image. This value controls how fast it should animate in milliseconds.'),
      '#default_value' => $this->options['lightbox_transition_speed'],
    ];
    $form['max_scale_ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max scale ratio'),
      '#description' => $this->t("Sets the maximum scale ratio for images. If you don't want Galleria to upscale any images, set this to 1. Leaving it blank will allow any scaling of the images."),
      '#default_value' => $this->options['max_scale_ratio'],
    ];
    $form['min_scale_ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Min scale ratio'),
      '#description' => $this->t("Sets the minimum scale ratio for images. F.ex, if you don’t want Galleria to downscale any images, set this to 1."),
      '#default_value' => $this->options['min_scale_ratio'],
    ];
    $form['overlay_opacity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overlay opacity'),
      '#description' => $this->t('This sets how much opacity the overlay should have when calling showLightbox().'),
      '#default_value' => $this->options['overlay_opacity'],
    ];
    $form['overlay_background'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overlay background'),
      '#description' => $this->t('This defines the overlay background color when calling showLightbox().'),
      '#default_value' => $this->options['overlay_background'],
    ];
    $form['pause_on_interaction'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause on interaction'),
      '#description' => $this->t('During playback, Galleria will stop the playback if the user presses thumbnails or any other navigational links.'),
      '#default_value' => $this->options['pause_on_interaction'],
    ];
    $form['popup_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Popup links'),
      '#description' => $this->t('Checking this box will open any image links in a new window.'),
      '#default_value' => $this->options['popup_links'],
    ];
    $form['preload'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preload'),
      '#description' => $this->t("Defines how many images Galleria should preload in advance. Please note that this only applies when you are using separate thumbnail files. Galleria always cache all preloaded images. <ul><li>%2 preloads the next 2 images in line.</li><li>%all forces Galleria to start preloading all images. This may slow down client.</li><li>%0 will not preload any images</li></ul>",
        [
          '%2' => '2',
          '%all' => 'all',
          '%0' => '0',
        ]
      ),
      '#default_value' => $this->options['preload'],
    ];
    $form['queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Queue'),
      '#description' => $this->t("Galleria queues all activation clicks (next/prev & thumbnails). You can see this effect when, for example, clicking %next many times. If you don't want Galleria to queue, then uncheck the box.", ['%next' => t('next')]),
      '#default_value' => $this->options['queue'],
    ];
    $form['responsive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Responsive'),
      '#description' => $this->t("Sets Galleria in responsive mode."),
      '#default_value' => $this->options['responsive'],
    ];
    $form['show'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Show'),
      '#description' => $this->t("This defines what image index to show at first. If you have left the %history box checked, then a permalink will override this number.", ['%history' => t('History permalinks')]),
      '#default_value' => $this->options['show'],
    ];
    $form['show_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show info'),
      '#description' => $this->t('Set this to false if you do not wish to display the caption.'),
      '#default_value' => $this->options['show_info'],
    ];
    $form['show_counter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show counter'),
      '#description' => $this->t('Set this to false if you do not wish to display the counter.'),
      '#default_value' => $this->options['show_counter'],
    ];
    $form['show_imagenav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show image navigation'),
      '#description' => $this->t('Set this to false if you do not wish to display the image navigation (next/prev arrows).'),
      '#default_value' => $this->options['show_imagenav'],
    ];
    $form['swipe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Swipe'),
      '#description' => $this->t('Enables a swipe movement for flicking through images on touch devices.'),
      '#default_value' => $this->options['swipe'],
    ];
    $form['thumb_crop'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumb crop'),
      '#description' => $this->t('Same as imageCrop, but for thumbnails.'),
      '#default_value' => $this->options['thumb_crop'],
      '#options' => $crop_options,
    ];
    $form['thumb_fit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Thumb fit'),
      '#description' => $this->t("If this is set to ‘true’, all thumbnail containers will be shrinked to fit the actual thumbnail size. This is useful if you have thumbnails of various sizes and will then float nicely side-by-side. This is only relevant if thumbCrop is set to anything else but ‘true’. If you want all thumbnails to fit inside a container with predefined width & height, set this to ‘false’."),
      '#default_value' => $this->options['thumb_fit'],
    ];
    $form['thumb_margin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thumb margin'),
      '#description' => $this->t('Same as %imagemargin, but for thumbnails.', ['%imagemargin' => $this->t('Image margin')]),
      '#default_value' => $this->options['thumb_margin'],
    ];
    $form['thumb_quality'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumb quality'),
      '#description' => $this->t("Defines if and how IE should use bicubic image rendering for thumbnails.<ul><li>%auto uses high quality if image scaling is moderate.</li><li>%false will not use high quality (better performance).</li><li>%true will force high quality renedring (can slow down performance)</li></ul>",
        [
          '%auto' => t('Auto'),
          '%false' => t('False'),
          '%true' => t('True'),
        ]
      ),
      '#default_value' => $this->options['thumb_quality'],
      '#options' => [
        'auto' => $this->t('Auto'),
        0 => $this->t('False'),
        1 => $this->t('True'),
      ],
    ];
    $form['thumbnails'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnails'),
      '#description' => $this->t("Sets the creation of thumbnails. If set to %false, then Galleria will not create thumbnails. If you set this to %empty, Galleria will create empty spans with the className %img instead of thumbnails.",
        [
          '%empty' => $this->t('Empty'),
          '%image' => $this->t('img'),
          '%false' => $this->t('False'),
        ]
      ),
      '#default_value' => $this->options['thumbnails'],
      '#options' => [
        'empty' => $this->t('Empty'),
        'numbers' => $this->t('Numbers'),
        0 => $this->t('False'),
        1 => $this->t('True'),
      ],
    ];
    $form['touch_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Touch transition'),
      '#description' => $this->t('Defines a different transition when a touch device is detected.'),
      '#default_value' => $this->options['touch_transition'],
      '#options' => $transition_options,
    ];
    $form['transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Transition'),
      '#description' => $this->t("The transition that is used when displaying the images.<ul><li>%fade will fade between images.</li><li>%flash will fade into the background color between images.</li><li>%slide will slide the images using the Galleria easing depending on image position.</li><li>%fadeslide will fade between images and slide slightly at the same time.</li></ul>",
        [
          '%fade' => $this->t('Fade'),
          '%flash' => $this->t('Flash'),
          '%slide' => $this->t('Slide'),
          '%fadeslide' => $this->t('Fade/Slide'),
        ]
      ),
      '#default_value' => $this->options['transition'],
      '#options' => $transition_options,
    ];
    $form['transition_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transition speed'),
      '#description' => $this->t("The milliseconds used when applying the transition."),
      '#default_value' => $this->options['transition_speed'],
    ];
    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#description' => $this->t('This will set a width to the gallery.'),
      '#default_value' => $this->options['width'],
      '#required' => TRUE,
    ];

    $form['advanced'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Advanced settings'),
      '#description' => $this->t('WARNING: Some of these settings will pass raw JavaScript to the browser, so should be used with care. See the %docs for more information on their usage.',
        [
          '%docs' => Link::fromTextAndUrl($this->t('documentation'), Url::fromUri('https://docs.galleria.io/collection/25-options', ['attributes' => ['target' => '_blank']]))->toString(),
        ]
      ),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['advanced']['history'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('History permalinks'),
      '#description' => $this->t('Add permalinks to all images in the gallery.'),
      '#default_value' => $this->options['advanced']['history'],
    ];

    $form['advanced']['data_config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data config'),
      '#description' => $this->t('This javascript function configures how the data should be extracted from the source. It should return an object that will blend in with the default extractions. WARNING: Raw JavaScript will be passed here.'),
      '#default_value' => $this->options['advanced']['data_config'],
    ];
    $form['advanced']['data_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data image selector'),
      '#description' => $this->t('The selector Galleria should look for in the HTML source. Defaults to %img and there is rarely any reason to change this.', ['%img' => 'img']),
      '#default_value' => $this->options['advanced']['data_selector'],
    ];
    $form['advanced']['data_source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data source'),
      '#description' => $this->t('This is where Galleria finds the data depending on data_type. It defaults to the target selector, which is the same element that was used in the jQuery plugin.'),
      '#default_value' => $this->options['advanced']['data_source'],
    ];
    $form['advanced']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('If checked, the slide show will throw errors when something is not right.'),
      '#default_value' => $this->options['advanced']['debug'],
    ];
    $form['advanced']['extend'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extend'),
      '#description' => $this->t("This function is used to extend the init function of the theme. Use this to quickly add minor customizations to the theme. The first argument is the options object, and the scope is always the Galleria gallery, just like the theme's init() function. WARNING: Raw JavaScript will be passed here."),
      '#default_value' => $this->options['advanced']['extend'],
    ];
    $form['advanced']['keep_source'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep source'),
      '#description' => $this->t('If checked, the source HTML will be left intact, which will also create clickable images of each image inside the source. Useful for building custom thumbnails and still have galleria control the gallery.'),
      '#default_value' => $this->options['advanced']['keep_source'],
    ];
    $form['advanced']['avoid_flash_of_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Avoid flash of content'),
      '#description' => $this->t('If checked, then the images of the Galleria slide show will be hidden by JavaScript in the page header, so that there is no flash of content on the page load.'),
      '#default_value' => $this->options['advanced']['avoid_flash_of_content'],
    ];
    $form['advanced']['strip_images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strip images'),
      '#description' => $this->t('If checked, then the images will be stripped from the original view query. Disabling it can be useful to add rich HTML captions.'),
      '#default_value' => $this->options['advanced']['strip_images'],
    ];

    return $form;
  }

}
