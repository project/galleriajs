
/**
 *  @file
 *  Initiate the Galleria plugin.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.galleriajs = {
    attach: function (context) {
      for (id in drupalSettings.galleriajs) {
        $('#' + id + ':not(.galleriajs-processed)', context).addClass('galleriajs-processed').each(function () {
          var settings = drupalSettings.galleriajs[$(this).attr('id')];
          // Eval settings that are functions.
          if (settings['extend']) {
            var extend = settings['extend'];
            eval("settings['extend'] = " + extend);
          }
          if (settings['dataConfig']) {
            var dataConfig = settings['dataConfig'];
            eval("settings['dataConfig'] = " + dataConfig);
          }

          // Load the theme.
          Galleria.loadTheme(settings['themePath']);

          // Fire up the gallery.
          $(this).galleria(settings);

        });
      }
    }
  };
}(jQuery, Drupal, drupalSettings));
