<?php

/**
 * @file
 * Theme & preprocess functions for the galleriajs.
 */

use Drupal\Component\Utility\Html;

/**
 * Galleriajs: Theme the main frame wrapper.
 *
 * @ingroup vss_theme
 */
function template_preprocess_galleriajs_main_frame(&$vars) {
  $vars['settings'] = $vars['view']->style_plugin->options;
  $vars['skin'] = 'default';
  $vars['slideshow'] = '';

  $view = $vars['view'];
  $view_element_name = (isset($view->element['#name'])) ? $view->element['#name'] : '';
  $vars['vss_id'] = $view_element_name . '-' . $view->current_display;

  if ($vars['settings']['advanced']['strip_images']) {
    // Initialize our $images array.
    $vars['images'] = [];

    $vars['original_rows'] = $vars['rows'];
    // Strip all images from the $rows created by the original view query.
    foreach ($vars['rows'] as $id => $item) {
      $item_rendered = \Drupal::service('renderer')->render($item);
      preg_match('@(<\s*img\s+[^>]*>)@i', $item_rendered, $matches);
      if (@$image = $matches[1]) {
        // If our image is in an anchor tag, use its URL.
        preg_match('@<\s*a\s+href\s*=\s*"\s*([^"]+)\s*"[^>]*>[^<]*' . preg_quote($image) . '[^<]*<\s*/a\s*>@i', $item_rendered, $urls);
        if (isset($urls[1])) {
          $url = $urls[1];
        }
        else {
          // Otherwise link to the original image or the front page instead.
          preg_match('@src\s*=\s*"([^"]+)"@i', $image, $urls);
          $url = isset($urls[1]) ? $urls[1] : url('<front>');
        }

        // Ensure the link for the original image is preserved.
        // $url has already been url'ized.
        $vars['rows'][$id] = '<a href="' . $url . '">' . $image . '</a>';

        // Add the image to our image array to display.
        $vars['images'][$id] = $image;
      }
    }
  }

  _galleriajs_add_js($vars['settings'], $vars);

  $vars['attributes']['class'][] = 'galleriajs-images';
  $vars['attributes']['class'][] = 'galleria';
  if ($vars['settings']['advanced']['avoid_flash_of_content']) {
    $vars['attributes']['class'][] = 'galleriajs-hidden';
  }

  $vars['#attached']['library'][] = 'galleriajs/galleriajs';
}

/**
 * Helper function to build and pass Galleria settings to Drupal.
 */
function _galleriajs_add_js($options, &$vars) {
  static $loaded_themes;

  // Keep track of which Galleria plugin JS themes have already been loaded.
  if (!isset($loaded_themes)) {
    $loaded_themes = [];
  }

  // Load our galleria js.
  $vars['#attached']['library'][] = 'galleriajs/galleria.js';

  if ($options['advanced']['history']) {
    $vars['#attached']['library'][] = 'galleriajs/galleria.plugins.history';
  }

  // Process Galleria settings.
  $settings = [
    'autoplay' => ($options['autoplay']) ? (($options['autoplay_ms']) ? (int) $options['autoplay_ms'] : TRUE) : FALSE,
    'carousel' => (bool) $options['carousel'],
    'carouselFollow' => (bool) $options['carousel_follow'],
    'carouselSpeed' => (int) Html::escape($options['carousel_speed']),
    'carouselSteps' => ($options['carousel_steps'] == 'auto') ? $options['carousel_steps'] : (int) $options['carousel_steps'],
    'clicknext' => (bool) $options['clicknext'],
    'debug' => (bool) $options['advanced']['debug'],
    'dummy' => Html::escape($options['dummy']),
    'easing' => Html::escape($options['easing']),
    'fullscreenCrop' => (bool) $options['fullscreen_crop'],
    'fullscreenDoubleTap' => (bool) $options['fullscreen_double_tap'],
    'fullscreenTransition' => (bool) $options['fullscreen_double_tap'],
    'height' => (float) Html::escape($options['height']),
    'idleMode' => (bool) $options['idle_mode'],
    'idleTime' => (int) Html::escape($options['idle_time']),
    'imageCrop' => ($options['image_crop'] == 'width' || $options['image_crop'] == 'height') ? $options['image_crop'] : (bool) $options['image_crop'],
    'imageMargin' => (int) Html::escape($options['image_margin']),
    'imagePan' => (bool) $options['image_pan'],
    'imagePanSmoothness' => (int) $options['image_pan_smoothness'],
    'imagePosition' => Html::escape($options['image_position']),
    'keepSource' => (bool) $options['advanced']['keep_source'],
    'layerFollow' => (bool) $options['layer_follow'],
    'lightbox' => (bool) $options['lightbox'],
    'lightboxFadeSpeed' => (int) Html::escape($options['lightbox_fade_speed']),
    'lightboxTransitionSpeed' => (int) Html::escape($options['lightbox_transition_speed']),
    'maxScaleRatio' => (int) $options['max_scale_ratio'],
    'minScaleRatio' => (int) $options['min_scale_ratio'],
    'overlayOpacity' => (float) $options['overlay_opacity'],
    'overlayBackground' => Html::escape($options['overlay_background']),
    'pauseOnInteraction' => (bool) $options['pause_on_interaction'],
    'popupLlinks' => (bool) $options['popup_links'],
    'preload' => ($options['preload'] == 'all') ? 'all' : (int) $options['preload'],
    'queue' => (bool) $options['queue'],
    'responsive' => (bool) $options['responsive'],
    'show' => (int) $options['show'],
    'showInfo' => (bool) $options['show_info'],
    'showCounter' => (bool) $options['show_counter'],
    'showImagenav' => (bool) $options['show_imagenav'],
    'swipe' => (bool) $options['swipe'],
    'thumbCrop' => ($options['thumb_crop'] == 'width' || $options['thumb_crop'] == 'height') ? $options['thumb_crop'] : (bool) $options['thumb_crop'],
    'thumbFit' => (bool) $options['thumb_fit'],
    'thumbMargin' => (int) Html::escape($options['thumb_margin']),
    'thumbQuality' => ($options['thumb_quality'] == 'auto') ? 'auto' : (bool) $options['thumb_quality'],
    'thumbnails' => ($options['thumbnails'] == 'empty' || $options['thumbnails'] == 'numbers') ? $options['thumbnails'] : (bool) $options['thumbnails'],
    'touchTransition' => Html::escape($options['touch_transition']),
    'transition' => Html::escape($options['transition']),
    'transitionSpeed' => (int) $options['transition_speed'],
    'width' => ($options['width'] == 'auto') ? $options['width'] : (int) Html::escape($options['width']),
  ];

  if ($options['theme'] == 'custom' && !empty($options['custom_theme_options']['custom_theme']) && !empty($options['custom_theme_options']['custom_theme_path'])) {
    $theme = Html::escape($options['custom_theme_options']['custom_theme']);
    $theme_path = base_path() . $options['custom_theme_options']['custom_theme_path'];
  }
  else {
    $theme_path = base_path() . "libraries/galleria/dist/themes/{$options['theme']}/galleria.{$options['theme']}.min.js";
  }

  // Load the Galleria theme.
  if (!isset($loaded_themes[$theme_path])) {
    $settings['themePath'] = $theme_path;
    $loaded_themes[$theme_path] = TRUE;
  }

  // Process advanced settings.
  if (isset($options['advanced']['extend']) && !empty($options['advanced']['extend'])) {
    $settings['advanced']['extend'] = $options['advanced']['extend'];
  }
  if (isset($options['advanced']['data_config']) && !empty($options['advanced']['data_config'])) {
    $settings['dataConfig'] = $options['advanced']['data_config'];
  }
  if (isset($options['advanced']['data_source']) && !empty($options['advanced']['data_source'])) {
    $settings['dataSource'] = $options['advanced']['data_source'];
  }

  // Load galleria settings.
  $vars['#attached']['drupalSettings']['galleriajs']['galleriajs-images-' . $vars['vss_id']] = $settings;
}
